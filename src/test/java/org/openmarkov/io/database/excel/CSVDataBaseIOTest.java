/*
* Copyright 2011 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/

package org.openmarkov.io.database.excel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.io.database.CaseDatabase;
import org.openmarkov.core.model.network.Variable;


public class CSVDataBaseIOTest {

	// Attributes
	private String fileName = "/csv/Cataract-Data2.csv";
	
	final private String[] variablesNames = {"id", "cataract", "operation", "success", "sex", 
			"age", "fupDur", "opType", "valDur", "cexDur", "breakType", 
			"numBreaks", "bPVR", "caPVR", "cpPVR", "pvr", "smlBreak", 
			"medBreak", "lrgBreak", "supt", "inft", "supn", "infn", "ante", 
			"post", "loc48", "loc57", "loc48rrd", "loc57rrd", "va", "pvd", "vh", 
			"quad", "fovea", "fupVa", "fupRetOff", "fupOil", "phthisis"};
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testOpenDBFile() {
		try {
		    CSVDataBaseIO databaseIO = new CSVDataBaseIO ();
		    URL file = getClass().getResource (fileName);
			CaseDatabase database = databaseIO.load(file.getFile ());
			List<Variable> variables = database.getVariables();
			// test number of variables
			assertEquals(variablesNames.length, variables.size());
			// test variables names
			boolean nameContained;
			for(Variable variable : variables) {
				nameContained = false;
				for(String name : variablesNames) {
					if (variable.getName().contentEquals(name)) {
						nameContained = true;
						break;
					}
				}
				assertTrue(nameContained);
			}
			// test values
			int[][] data = database.getCases ();
			assertEquals(1, data[1][0]);
			assertEquals(12, data[12][0]);
			assertEquals(19, data[19][0]);
			assertEquals(0, data[2][1]);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
